//
//  PreferencesController.m
//  AquaticPrime Developer
//
//  Created by Dave Carlton on 10/16/10.
//  Copyright (c) 2010 Polymicro Systems. All rights reserved.
//

#import "PreferencesController.h"


@implementation PreferencesController
@synthesize emailAccounts;

static PreferencesController *shared;

- (id)init {
    if (shared) 
	{
        [self autorelease];
        return shared;
    }
    if ((self = [super init])) {
        // Initialization code here.
		shared = self;
    }
    
    return self;
}

- (void)dealloc {
    // Clean-up code here.
	[prefsWindow release];
	[tabView release];
	[tabToolBar release];
	[updateText release];
	[[NSUserDefaultsController sharedUserDefaultsController] removeObserver:self forKeyPath:@"values.SULastCheckTime"];
    [super dealloc];
}

- (void)awakeFromNib {
	
	[[NSUserDefaultsController sharedUserDefaultsController] addObserver:self forKeyPath:@"values.SULastCheckTime" options:0 context:NULL];
	
	// Find Email accounts
	NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(																   NSLibraryDirectory, NSUserDomainMask, YES);
	NSString *libraryPath = [searchPaths objectAtIndex:0];
	NSString *mailPrefsPath = [[libraryPath stringByAppendingPathComponent:@"Preferences"] stringByAppendingPathComponent:@"com.apple.mail.plist"];
	NSDictionary *mailPreferences = [NSDictionary dictionaryWithContentsOfFile:mailPrefsPath];
	emailAccounts = [mailPreferences valueForKey:@"DeliveryAccounts"];

	NSInteger emailProcess = [[NSUserDefaults standardUserDefaults] integerForKey:@"MailProcess"];
	if (emailProcess == 1) {
		[mailProcessMatrix selectCellAtRow:0 column: 0];
		[appleMailButton setIntValue:1];
		[internalMailButton setIntValue:0];
		[mailServer setEnabled:NO];
		[accountsTable setEnabled:NO];
	} else {
		[mailProcessMatrix selectCellAtRow:1 column: 0];
		[appleMailButton setIntValue:0];
		[internalMailButton setIntValue:1];
		[mailServer setEnabled:YES];
		[accountsTable setEnabled:YES];	
	}

#ifdef MACAPPSTORE
	NSArray *tbItems = [tabToolBar items];
	NSEnumerator *enumerator = [tbItems objectEnumerator];
	
	id anItem;
	NSInteger i =0;
	while (anItem = [enumerator nextObject]) {
		if ([anItem itemIdentifier] == [toolbarItemUpdate itemIdentifier]) {
			[tabToolBar removeItemAtIndex:i];
			break;
		}
		i++;
	}
#endif
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (object == [NSUserDefaultsController sharedUserDefaultsController] && ([keyPath hasSuffix:@"SULastCheckTime"]))
		[self refreshUpdateDate:self];
	else
		[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

- (NSArray *)toolbarSelectableItemIdentifiers:(NSToolbar *)toolbar {
	
	NSArray* theItems = [toolbar items];
	NSInteger theCount = [theItems count];
	NSInteger index;
	NSMutableArray* theArray = [NSMutableArray arrayWithCapacity:theCount];
	
	for(index=0; index<theCount; index++)
		[theArray addObject:[[theItems objectAtIndex:index] itemIdentifier]];
	
	NSLog(@"Selectables: %@", theArray);
	
	return theArray;
}

- (NSToolbarItem*)toolbarItemForIdentifier:(NSString*)id
{
	NSToolbarItem*		outToolbarItem = nil;
	NSArray*			toolbarItems = [tabToolBar visibleItems];
	int					index, count = [toolbarItems count];
	
	for( index=0; index < count; index++ )
	{
		outToolbarItem = [toolbarItems objectAtIndex:index];
		if( [[outToolbarItem itemIdentifier] compare:id] == NSOrderedSame )
			break;
		if( [[outToolbarItem label] compare:id] == NSOrderedSame )
			break;
		outToolbarItem = nil;
	}
	
	return outToolbarItem;
}


- (IBAction)runPreferences:(id)sender
{
	NSString*	startToolID = [[NSUserDefaults standardUserDefaults] objectForKey:@"PrefToolSelected"];
	
	if( tabToolBar ) 
		[self clickTool:[self toolbarItemForIdentifier:startToolID]];
	[self refreshUpdateDate:self];
	if( prefsWindow ) 
		[prefsWindow makeKeyAndOrderFront:sender];
}

- (IBAction)clickTool:(NSToolbarItem*)sender
{
	NSLog(@"Clicked Tool Label:%@ ID:%@", [sender label], [sender itemIdentifier]);
	if (sender) {
		if( tabView ) 
			[tabView selectTabViewItemWithIdentifier: [sender label]];
		if( tabToolBar ) 
			[tabToolBar setSelectedItemIdentifier: [sender itemIdentifier]];
		[[NSUserDefaults standardUserDefaults] setObject:[sender itemIdentifier] forKey:@"PrefToolSelected"];
	} else {
		NSArray *tbItems = [tabToolBar items];
		NSString *id = [[tbItems objectAtIndex:0] itemIdentifier];
		[tabToolBar setSelectedItemIdentifier:id];
		[tabView selectTabViewItemWithIdentifier: @"General"];
		[[NSUserDefaults standardUserDefaults] setObject:id forKey:@"PrefToolSelected"];
	}
			
	[self refreshUpdateDate:sender];
	
	NSView*		theTabView = [[tabView selectedTabViewItem] view]; 
	NSRect		theBottomFrame = [[theTabView viewWithTag:99] frame]; // Special item
	NSRect		theTabFrame = [theTabView frame];
	NSRect		theWindowFrame = [[tabView window] frame];
	NSRect		theContentFrame = [[tabView window] contentRectForFrameRect:theWindowFrame];
	NSPoint		theNewSize; // = theTabFrame.size;
	
	float		theToolbarHeight = -(theWindowFrame.size.height - theContentFrame.size.height);
	float		theNewHeight = (theWindowFrame.size.height - (theTabFrame.size.height - theBottomFrame.origin.y)) + theToolbarHeight;
	float		theDeltaHeight = theWindowFrame.size.height - theNewHeight;
	float		theDeltaOrigin = theWindowFrame.origin.y + theNewHeight;
	
	theNewSize.x = theBottomFrame.size.width;
	theNewSize.y = theTabFrame.size.height - theBottomFrame.origin.y;
	
	theWindowFrame.size.width = theBottomFrame.size.width;
	theWindowFrame.size.height = theDeltaHeight; // theTabFrame.size.height - theBottomFrame.origin.y;
	theWindowFrame.origin.y = theDeltaOrigin;
	
	//[[tabView window] setContentSize:theWindowFrame.size];
	[[tabView window] setFrame:theWindowFrame display:YES animate:YES];
}


- (IBAction)refreshUpdateDate:(id)sender
{
	NSDate*		theDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"SULastCheckTime"];
	NSString*	theLastCheckFormat = NSLocalizedString(@"Last Check: %@", @"Prefs Window Update Tab Last Check Prompt Format for last update check date");
	NSString*	theUpdateString = NSLocalizedString(@"Never checked for an update.", @"Prefs Window Update Tab Last Check Prompt default when no update check date is found");
	
	CFLocaleRef			thisLocale = CFLocaleCopyCurrent();
	CFDateFormatterRef	thisFormat = CFDateFormatterCreate( kCFAllocatorDefault, thisLocale, kCFDateFormatterFullStyle, kCFDateFormatterFullStyle );
	CFStringRef			thisDate = NULL;
	
	if( theDate ) {
		thisDate = CFDateFormatterCreateStringWithDate( kCFAllocatorDefault, thisFormat, (CFDateRef)theDate );
		theUpdateString = [NSString stringWithFormat:theLastCheckFormat, (NSString*)thisDate];
		CFRelease( thisDate );
	}
	
	if( updateText ) [updateText setStringValue:theUpdateString];
	
	CFRelease( thisFormat );
	CFRelease( thisLocale );	
}

- (IBAction)provideFeedback:(id)sender
{
	CFURLRef	feedbackURL = CFURLCreateWithString( kCFAllocatorDefault, CFCopyLocalizedString(CFSTR("http://polymicro.net/products/aqprime/feedback/"), CFSTR("URL used when user selects Provide User Feedback")), NULL );
	CFURLRef	launchURL;
	
	LSOpenCFURLRef( feedbackURL, &launchURL );
    CFRelease(feedbackURL);
}

- (IBAction)mailProcessClick:(id)sender
{
	NSButtonCell *selCell = [sender selectedCell];
	if(selCell == appleMailButton) {
		[[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"MailProcess"];
		[mailServer setEnabled:NO];
		[accountsTable setEnabled:NO];
	} else {
		[[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"MailProcess"];
		[mailServer setEnabled:YES];
		[accountsTable setEnabled:YES];	
	}
}

#pragma mark Table Delegates
- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
	return [emailAccounts count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
	NSString *account = [[emailAccounts objectAtIndex:row] valueForKey:@"Hostname"];
	return account;
}

- (void)tableView:(NSTableView *)tableView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{

	
}

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
	NSInteger selectedRow = [accountsTable selectedRow];
	if (selectedRow != -1) {
		NSString *account = [[emailAccounts objectAtIndex:selectedRow] valueForKey:@"Hostname"];
		[hostName setStringValue:account];
		NSString *port = [[emailAccounts objectAtIndex:selectedRow] valueForKey:@"PortNumber"];
		if ([port length]) {
			[hostPort setStringValue:port];
		} else
			[hostPort setStringValue:@"25"];
		[[NSUserDefaults standardUserDefaults] setObject: account forKey: @"MailServer"];
		[[NSUserDefaults standardUserDefaults] setObject: port	forKey: @"MailPort"];
	}
}

@end
